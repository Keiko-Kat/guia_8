#include "Ejer_1.h"
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <chrono>
#include <math.h>
#define MAX 1000000
using namespace std;



int main (int argc, char**argv) {
	//se reciben parametros de terminal al abrir el programa
	//se comprueba que no falten parametros
	if(argc < 3){
		//si los parametros entregados por terminal son menos que los necesarios
	    cout<<"||Faltan parámetros||"<<endl;
	    //se envia un mensaje de error y se cierra el programa
	    return -1;
	}
	//se ingresa el tercer valor dado por terminal a un array de char
	char *opt = argv[2];
	int num;
	//se comprueba que la segunda entrada en terminal sea un int
	try{
		num = stoi(argv[1]);
	}catch (const std::invalid_argument& e){ 
		//en el caso de no ser transformable a un int, se envia un mensaje de error y se cierra el programa
		cout << "||El primer parámetro debe ser un número||" << endl; 
		exit(1); 
	}
    int opi = static_cast<int>(opt[0]);
    //y finalmente los valores se ingresan en dos distintos int, uno define la cantidad de numeros con la que se trabaja
    //el otro define que clase de salida desea ver el usuario
    Ejercicio e = Ejercicio();
    //se crea un objeto de tipo ejercicio, que contiene las funciones basicas del programa
    string in;
    bool is_true, valid_in = false;
    int temp = 1, bur_men = 0, bur_may = 0, ins_sim = 0, ins_bin = 0, selecc = 0, shel= 0, quick= 0;
    //se crean diversas variables que mantendran el programa en funcionamiento
	clock_t time = 0;
    tm time_now;
    int time_rand = time_now.tm_isdst;
    srand(time_rand);
    //se crea una variable tiempo tipo tm para asegurar una semilla nueva para cada iteracion del programa
    //time_rand se vuelve la cantidad de segundos que han pasado desde el 1 de enero de 1970
    //asegurando un numero diferente cada vez que se corre el programa
    
    while(temp != 0){
		//temp = 1 es utilizado para comprobar que el num ingresado se encuentre entre los limites establecidos por el programa
		if(num > 0 && num <= MAX){
			//si se encuentra en el rango, temp se iguala a 0 y se pasa a la siguiente fase del programa
			temp = 0;
		}else{
			//si es un ingreso no valido, se pide un nuevo ingreso
			cout<<"Tamaño no valido, ingrese nuevo tamaño del arreglo [0, 1.000.000]"<<endl;
			cout<<"   ~~~> ";
			//se recibe en un string desde el teclado
			getline(cin, in);
			//y se pasa por una funcion para validar el ingreso de un int
			is_true = e.validar_int(in);		
			//la funcion regresa un booleano, de ser is_true true, num es reemplazado por el nuevo ingreso
			if(is_true == true){
				num = stoi(in);
			}else{
				//si is_true es false se da un mensaje de error y se regresa al inicio
				cout<<"Ingreso no válido"<<endl;
				cout<<"Intente nuevamente"<<endl;
			}
			//una vez num sea midificado (o no), se retorna al inicio del while, comprobando si el nuevo num es valido
			//si el nuevo num no cumple las condiciones el ciclo continua
		}
	}
    
    //una vez que se confirma que num es un numero valido, se crean dor array para correr el programa
    //y se crean otros 6 array para conservar el desarrollo del programa
    int array[num], arr[num], arr_bmen[num], arr_bmay[num], arr_in[num], arr_inbin[num], arr_sel[num], arr_shel[num];
    
    //luego se comprueba la validez del ingreso char del inicio del programa
	while(valid_in != true){
	//opi es igual al valor ASCII de la letra ingresada, y se le compara a los valores de "s" y "n"
		if(opi == 110 || opi == 115){
			//si el char es "s" o "n", el booleano valid_in se vuelve verdadero y cierra el while
			valid_in = true;
		}else{
			//si el char no corresponde, se pide otro ingreso
			cout<<"ingrese nueva opcion valida:"<<endl;
			cout<<"   ~~~> ";
			getline(cin, in);
			//se lee el string y se vuelve un array de char
			opt = const_cast<char*>(in.c_str());
			//se toma el primer char y se guarda su valor ASCII como int 
			opi = static_cast<int>(opt[0]);
			//una vez hecho el while se devuelve a la comprovacion de valores
		}
	}
    //en cuanto se valida la variable "opi" se genera el primer array, de froma completamente aleatoria
    
    e.relleno(num, array);  
    
    //y luego, este array se copia sobre un segun do array de nombre "arr"
    //"arr" existe para "reiniciar" a "array" despues de cada funcion
    e.rearrange(array, arr, num);
    
    //burbuja menor	
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.burbuja_menor(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	bur_men = clock() - time;
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_bmen, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);
	
	
	//burbuja mayor	
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.burbuja_mayor(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	bur_may = clock() - time;
	
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_bmay, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);
	
	
	//insercion
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.insercion(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	ins_sim = clock() - time;
	
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_in, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);


	//insercion binaria
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.insercion_bin(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	ins_bin = clock() - time;
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_inbin, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);
	
	//selección
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.seleccion(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	selecc = clock() - time;
	
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_sel, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);
	
	
	//shell
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.Shell(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	shel = clock() - time;
	
	//el resultado de la funcion se copia en un array temporal
	e.rearrange(array, arr_shel, num);
	//y luego array se sobreescribe a su estado inicial
	e.rearrange(arr, array, num);
	
	
	//Quicksort
    //lo primero que se hace es tomar el tiempo actual del computador en milisegundos 
	time = clock();
	//luego se ingresa array y num a la funcion llamada desde el objeto "e"
	e.quicksort(array, num);
	//para al final restar el tiempo "time" que se tomo antes de iniciar la funcion
	//del tiempo actual del computador al momento de acabar la funcion, y se guarda el tiempo en una variable tipo int
	quick = clock() - time;
	//ya que no existe necesidad de reiniciar "array" por ultima vez, el metodo quicksort no posee uno propio
	
	
	if(opi == 115){
		//si la opcion elegida fue "s" se imprime el array "arr" que continua con su orden original
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr[i]<<" ";
		}
		cout<<endl;
		cout<<endl;
	}		
	//sin importar la opcion elegida se imprime un mini "menú" que muestra el tiempo en milisegundos que tardó cada funcion individual en ordenar a array
    cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
 	cout<<"Metodo              || tiempo "<<endl;
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	cout<<"Burbuja Menor       || "<< bur_men<< " milisegundos"<<endl;
	cout<<"Burbuja Mayor       || "<< bur_may<< " milisegundos"<<endl;
	cout<<"Inserción           || "<< ins_sim<< " milisegundos"<<endl;
	cout<<"Inserción Binaria   || "<< ins_bin<< " milisegundos"<<endl;
	cout<<"Selección           || "<< selecc << " milisegundos"<<endl;
	cout<<"Shell               || "<<   shel << " milisegundos"<<endl;
	cout<<"Quicksort           || "<<  quick << " milisegundos"<<endl;
	cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
	cout<<endl;
	cout<<endl;
	
	if(opi == 115){		
		//si la opcion elegida fue "s" se imprimen los contenidos de cada array temporal de cada funcion
		cout<<"Burbuja Menor       || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_bmen[i]<<" ";
		}
		cout<<endl;
		cout<<"Burbuja Mayor       || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_bmay[i]<<" ";
		}
		cout<<endl;
		cout<<"Inserción           || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_in[i]<<" ";
		}
		cout<<endl;
		cout<<"Inserción Binaria   || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_inbin[i]<<" ";
		}
		cout<<endl;
		cout<<"Selección           || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_sel[i]<<" ";
		}
		cout<<endl;
		cout<<"Shell               || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<arr_shel[i]<<" ";
		}
		cout<<endl;
		cout<<"Quicksort           || ";
		for(int i = 0; i < num; i++){
			cout<<"a["<<i<<"] = "<<array[i]<<" ";
		}
		cout<<endl;
		cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
		
		cout<<endl;
	}
   
    return 0;
}