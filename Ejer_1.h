using namespace std;
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <chrono>
#include <math.h>
#define MAX 1000000

class Ejercicio {
		
	
	public:
        //La primera funcion publica de la clase ejercicio es "rearrange" una funcion de tipo void
       void rearrange(int arr[], int array[], int tamano){
            //Esta funcion recibe dor array tipo int
            //y a traves de un ciclo for llena el segundo array con los contenidos del primero
			for (int i = 0; i< tamano; i++){
				array[i] = arr[i];
			}
		}
        
        
        //la funcion de tipo bool validar_int recibe un string de entrada por terminal
		bool validar_int(string in){
			//para validar que la entrada sea un int
			bool is_true = true;
			//se crea is_true como una vaiable bool igual a true
			//y se ingresa en un ciclo for que dura mientras el string tenga nuevos caracteres
			for(int j = 0; j < in.size(); j++){
				//dentro del for se comparan los valores ASCII del ingreso con los valores 
				//que corresponden a los numeros de 0-9 y al caracter "-"
				if((in[j] < 48 || in[j] >57) && in[j] != 45){
				    //si cualquiera de los caracteres no es un numero valido o "-" la variable is_true se iguala a false 
					is_true = false;
				}
			}
			//la funcion retorna la variable is_true
			return is_true;
		}
		
		//relleno es una funcion de tipo void que recibe un array vacío y un int de tamaño
		void relleno(int tamano, int array[]){
		    //y se ingresa a un for que llena el array con numero random ente 1 y 100000
			for(int i = 0; i < tamano; i++){
				array[i] = rand() % 100000 +1; 
			}
		}
		
        //la primera funcion de ordenamiento es la funcion void burbuja menor
        void burbuja_menor(int array[], int tamano){
            //recibe un array y las dimensiones de este
			int temp, i, j;
			//se crean tres parametros tipo int y se ingresa a un for
			for (i = 1; i < tamano; i++){
			    //el primero for iguala i a 1 y continua haciendo i+1 en cada iteracion hasta quedar a una unidad de tamaño
				for(j = tamano-1; j >= i; j--){
				    //mientras que el segundo iguala j a tamaño-1, haciendo j-1 en cada iteracion hasta que j se iguala a i 
				    //esto quiere decir que el primer for recorre el array de forma ascendente y el segundo de forma descendente
					if(array[j-1] > array[j]){
					    //se pregunta si la posicion anterior a array[j] es mayor a esta
					    //de ser así, se iguala un temporal al contenido de array[j-1]
						temp = array[j-1];
						//luego informacion en la posicion j-1 es reemplazada por la de la posicion j
						array[j-1] = array[j];
						//y finalmente los contenidos que estaban en la posicion j-1 se guardan en la posicion j
						array[j] = temp;
					}
				}
			}
			//los ciclos se repiten hasta que ya no quede numero sin remover
		}

        //la siguiente funcion de ordenamiento es la funcion burbuja mayor
		void burbuja_mayor(int array[], int tamano){
			int temp, i, j;
			//se crean tres parametros tipo int y se ingresa a un for
			for (i = tamano-1; i >0; i--){
			    // primero se iguala i a tamaño-1, haciendo i-1 en cada iteracion hasta quedar a una unidad de 0
				for(j = 0; j < i; j++){
				    //el segundo for iguala j a 1 y continua haciendo j+1 en cada iteracion hasta que i se iguala a j
				    //funciona a la inversa que el burbuja menor
					if(array[j] > array[j+1]){
					    //si la informacion en la pisicion j+1 es menos a la posicion en j
					    //se guarda la informacion contenida en la posicion j en una variable temporal
						temp = array[j];
						//luego se reemplaza la informacion en la posicion j por la contenida en j+1
						array[j] = array[j+1];
						//y finalmente la informacion contenida en temporal se guarda en la posicion j+1
						array[j+1] = temp;
					}
				}
			}
			//los ciclos se repiten hasta que ya no quede numero sin remover
		}
        
        
        //la siguiente funcion es la funcion de insercion
        void insercion(int array[], int tamano){
			int temp, k, i;
			//se crean tres parametros tipo int y se ingresa a un for
			for(i = 1; i < tamano; i++){
			    //el for iguala i a 1 y hace i+1 en cada iteracion mientras que i < tamano
			    //la informacion contenida en la posicion i se guarda en un int temporal
				temp = array[i];
				//y k se iguala a i-1
				k = i-1;
				//con k se entra a un ciclo while que dura mientras k no sea negativo 
				//y mientras la informacion en la posicion k sea mayor a la guardada en temp
				while((k >= 0) && (temp < array[k])){
				    //dentro del while se toma la informacion de la posicion k y se guarda en la posicion k+1
					array[k+1] = array[k];
					//k se vuelve k-1
					k = k-1;
				}
				//y al salir del while se hace que la informacion de temp se guarde en la posicion k+1 
				array[k+1] = temp;
			}
			//los ciclos se repiten hasta que ya no quede numero que mover
		}

        //la siguiente funcion es la funcion para la insercion binaria
		void insercion_bin(int array[], int tamano){
			int temp, i, m, j, iz, der;
			//se crean seis parametros tipo int y se ingresa a un for
			
			for(i = 1; i < tamano; i++){
			    //el for iguala i a 1 y hace i+1 en cada iteracion mientras que i < tamano
			    //la informacion contenida en la posicion i se guarda en un int temporal
				temp = array[i];
				//la variable iz se iguala a 0 y la der se iguala a i-1
				iz = 0;
				der = i-1;
				//se entra a un ciclo while que se detiene cuando se cumple que iz > der
				while(iz <= der){
				    //se iguala m a la parte entera del promedio entre iz y der
					m = trunc((iz+der)/2);
					//y se pregunta si la informacion en temp es menor o igual a la informacion contenida en la posicion m
					if(temp <= array[m]){
					    //si la condicion se cumple der se hace m-1
						der = m-1;
					}else{
					//y si no se cumple, iz se vuelve m+1
						iz = m+1;
					}
				}
				//una vez terminado el while j se iguala a i-1
				j = i-1;
				//y se ingresa a un segundo while, que corre mientras j no sea menor a iz
				while(j >= iz){
				    //dentro del while, la informacion contenida en la posicion j se guarda en la posicion j+1
					array[j+1] = array[j];
					//y j se iguala a j-1
					j = j-1;
				}
				//acabado el segundo while la informacion contenida en temp se guarda en la posicion iz del array
				array[iz] = temp;
			}
			//los ciclos se repiten hasta que ya no quede numero que mover
		}
        
        //la ssiguiente funcion corresponde al metodo de seleccion
		void seleccion(int array[], int tamano){
			int i, menor, k, j;
			//se crean cuatro parametros tipo int y se ingresa a un for
			
			for(i=0; i < tamano-1; i++){
			    //el for iguala i a 1 y hace i+1 en cada iteracion mientras que i < tamano
			    //la informacion contenida en la posicion i se guarda en un int "menor"
				menor = array[i];
				//la variable k se iguala a i
				k=i;
				//se ingresa a un segundo for, que iguala j a i+1 y se hace j+1 hasta que j alcanza a tamaño
				for (j = i+1; j < tamano; j++){
				    //se pregunta si la informacion que se encuentra en la posicion j es menor que "menor"
					if(array[j] < menor){
					    //si se cumple la condicion se reemplaza la informaciond entro de menor, con la contenida en la posicion j
						menor  = array[j];
						//y se iguala k a j
						k = j;
					}
				}
				//cuando el segundo ciclo acaba se reemplaza la informacion en la posicion k por la que se encuentra en la posicion i
				array[k] = array[i];
				//y la posicion i se llena con la informacion contenida en menor
				array[i] = menor;
			}
			//los ciclos se repiten hasta que ya no quede numero que mover
		}

        //la penultima funcion es la que ejecuta el ordenamiento tipo shell
		void Shell(int array[], int tamano){
			int in, i, temp;
			bool band;
			//se crean tres parametros tipo int y uno tipo bool
			in = tamano+1;
			//in se iguala a tamaño +1 y se ingresa a un while
			while (in > 1){
			    //el ciclo corre mientras in sea mayor que 1
				in = trunc(in/2);
				//in se iguala a la parte entera de su mitad
				band = true;
				//la variable booleana se iguala a true y se ingresa a un segundo while
				while(band == true){
				    //el segundo ciclo corre mientras la variable booleana sea igual a true
					band = false;
					//lo primero que se hace es igualar band a false
					i = 0;
					//se iguala i a 0 y se ignresa a un tercer while
					while((in + i) < tamano){
					    //el tercer ciclo corre mientras la suma entre i e in sea menor a tamano
						if(array[i] > array[i+in]){
						    //se pregunta si la informacion en la posicion i es mayor que la que se encuentra en la posicion i+in
						    //la informacion guardada en la posicion i se guarda en temp
							temp = array[i];
							//la informacion de la posicion i se reemplaza ṕor la que se encuentra en la posicion i+in 
							array[i] = array[in+i];
							//la posicion i+in se llena con la informacion guardada en temp
							array[i+in] = temp;
							//y band se iguala a true
							band = true;
						}
						//independiente del resultado de la condicion se iguala i a i+1
						i = i+1;
					}
				}
			}
			//los ciclos se repiten hasta que ya no quede numero que mover
		}
		
		
		//la ultima funcion de ordenamiento es la funcion quicksort
		void quicksort(int array[], int tamano){
			int tope, ini, fin, pos, izq, der, temp;
			//se crean siete variables int 
			int pilamenor[tamano];
			int pilamayor[tamano];
			//se crean dos array tipo int extra para el funcionamiento de la funcion
			bool band;
			//se crea una variable tipo band
			//tope se iguala a 0
			tope = 0;
			//y se llena la primera posicion en las pilas, la menor se llena con 0 y la mayor se iguala a tamano-1
			pilamenor[tope] = 0;
			pilamayor[tope] = tamano-1;
			//y se ingresa a un while 
			while (tope >= 0){
			    //el ciclo continua mientras tope no tome valores negativos
			    //ini se iguala a la posicion tope de pilamenor
				ini = pilamenor[tope];
				//y fin se iguala a la posicion tope de pilamenor
				fin = pilamayor[tope];
				//tope se iguala a tope-1
				tope = tope - 1;
				// izq se iguala a ini, der se iguala a fin y pos sse iguala a ini
				izq = ini;
				der = fin;
				pos = ini;
				//band se iguala a true
				band = true;
				//y se abre un segundo while
				while(band == true){
				    //el ciclo dura mientras band sea igual a true
				    //y se abre un tercer while
					while((array[pos] <= array[der]) && (pos != der)){
					    //mientras la informacion en la posicion pos sea menor o igual a la informacion en la posicion der
					    //y mientras pos sea distinto de der, der se hace der-1
						der = der - 1;
					}
					//al salir del while se pregunta si pos es igual a der
					if(pos == der){
					    //si se cumple la condicion band se hace falso
						band = false;
					}else{
					    //si la condicion no se cummple la informacion contenida en la posicion pos se guarda en temp
						temp = array[pos];
						//la posicion pos se llena con la informacion en la posicion der
						array[pos] = array[der];
						//y la posicion der se iguala a la informacion contenida en temp
						array[der] = temp;
						//pos se iguala a der
						pos = der;
						//y se abre un ultimo while
						while((array[pos] >= array[izq]) && (pos!=izq)){
						    //el ciclo corre mientras que la informacione en la posicion pos no sea menor que la informacion en la posicion izq
						    //y mientras pos sea distinto de izq
						    //izq se hace izq + 1
							izq = izq + 1;
							//la informacion en la posicion pos se guarda en temp
							temp = array[pos];
							//la informacion en la posicion izq se guarda en pos
							array[pos] = array[izq];
							//la informacion en temp se guarda en izq
							array[izq] = temp;
							//y pos se iguala a izq
							pos = izq;
						}
					}
				}
				//acabando el segundo while se abren dos condicionales
				//la primera pregunta si ini es menor o igual a pos-1
				if(ini <= (pos - 1)){
				    //de cumplirse la codicional tope se iguala a tope+1
					tope = tope + 1;
					//la posicion tope en pilamenor se llena con ini
					pilamenor[tope] = ini;
					//y la posicion tope en pilamayor se llena con pos-1
					pilamayor[tope] = (pos-1);
				}
				//la segunda pregunta si fin es mayor o igual a pos+1
				if(fin >= (pos+1)){
				    //de cumplirse la codicional tope se iguala a tope+1
					tope = tope+1;
					//la posicion tope en pilamenor se llena con pos + 1
					pilamenor[tope] = (pos+1);
					//y la posicion tope en pilamayor se llena con fin
					pilamayor[tope] = fin;
				}
			}
			//los ciclos se repiten hasta que ya no quede numero que mover
		}
		
};